This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for gCube Smartgears Bom

## [v4.0.1-SNAPSHOT]
- Removed gxJRS
- Moved to gcube-bom 4.0.1-SNAPSHOT
- Updated classgraph version from 4.8.28 to 4.8.179 due to vulnerabilities

## [v4.0.0]

- Added common-scope 
- Moved to jakarta libraries
- Moved to java11 libraries


## [v3.0.0] - 2023-09-14

- Moved to smartgears 4.0.0 
- Enhanced ranges of common libraries
- Enhanced ranges of gx-rest libraries
- Enhanced ranges of authorization libraries
- Replaced common-utility-sg3 with common-utility-sg4
- Enhanced information-system-model version range
- Enhanced gcube-model version version range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range


## [v2.3.0] - 2023-03-09

- Enhanced information-system-model version range
- Enhanced gcube-model version lower bound of range
- Enhanced resource-registry-api lower bound of range
- Enhanced resource-registry-client lower bound of range
- Enhanced resource-registry-publisher lower bound of range
- Added common-utility-sg3


## [v2.2.0] - 2022-11-28

- Enhanced gcube-model version range
- Enhanced information-system-model version range
- Upgraded gcube-bom version to 2.1.0-SNAPSHOT


## [v2.1.1] - 2022-03-14

- Enhanced gcube-bom version


## [v2.1.0] - 2020-11-24

- Added missing new IS libs

## [v2.0.0] [r4.26.0] - 2020-11-11 

- Switched JSON management to gcube-jackson [#19283]


## [v1.2.0] [r4.21.0] - 2020-03-30

- Updated gcube-bom version


## [v1.1.0] [r4.17.0] - 2019-12-19

- Added version already declared in gcube-bom to avoid that some component does not inherits the version
- Removed -SNAPSHOT from dependencies lower bound of ranges


## [v1.0.2] [r4.15.0] - 2019-11-08

- Fixed distro files and pom according to new release procedure


## [v1.0.1] [r4.13.1] - 2019-02-26

- Added common-smartgears-utils dependency
- Added gxHTTP dependency


## [v1.0.0] [r4.2.0] - 2016-12-15

- First Release

